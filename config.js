var config = {
	baseUrl: "http://dev.listing.gr",
	errorNodeId: "loginError",
	loginFormId: "loginForm",
	saveUrlBlockId: "saveUrlBlock",
	loginBtnId: "loginBtn",
	emailFieldId: "email",
	passwordFieldId: "password",
	sendUrlBtnId: "sendUrlBtn",
	sendUrlBlockId: "sendUrlBlock",
	logoutConfirmationId: "logoutConfirmation",
	logoutConfirmBtnId: "logoutConfirmBtn",
	logoutNotConfirmBtnId: "logoutNotConfirmBtn",
	checkSelectionBlockId: "checkSelectionBlock",
	checkSelectionBtnId: "checkSelectionBtn",
	selectionStatusId: "selectionStatus",
	selectionVariantsId: "selectionVariants",
	statusMessageId: "status",
	statusSuccessId: "statusSuccess",
	successBlockId: "statusSavedBlock",
	closePopupBtnId: "closePopupBtn",
	loadingId: "loading",
	logoutBtnId: "logout",

	api: {
		auth: "/api/v1/users/auth",
		searchUrl: "/api/v1/search-url",
		addUrl: "/api/v1/add-url",
		checkSelectionUrl: "/api/v1/search-query"
	},

	status: {
		urlExist: 101,
		urlExistPending: 102,
		urlNotExist: 103,
		error: [400, 401, 402, 403, 404, 500]
	},

	messages: {
		tabUrl: "tab.url should be a string",
		loginError: "Please, enter correct email or password.",
		somethingWentWrong: "Somethis went wrong",
		selectionIncorrect: "No Data Selected"
	}
}