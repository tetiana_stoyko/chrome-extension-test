function ChromeExtensionUrlTracking() {

  var that = this;
  this.urlObj = {};
  var logoutBtn = document.getElementById(config.logoutBtnId);

  /**
    * Initial function. Add events
    */
  var init = function() {
    if (localStorage.api_key) {
      hideLoginForm(true);
      that.checkUrl();
      logoutBtn.style.display = 'block';
    }
    that.addEvents();
  }

  this.addEvents = function() {
    document.getElementById(config.loginBtnId).addEventListener('click', function(e) {
      e.preventDefault();
      var data = {
        email : document.getElementById(config.emailFieldId).value,
        password : document.getElementById(config.passwordFieldId).value
      };
      that.makeRequest('POST', config.baseUrl + config.api.auth, data, null, function(response) {
        that.loginCallback(response);
      })
    });

    document.getElementById(config.sendUrlBtnId).addEventListener('click', function(e) {
      if (that.urlObj)
        that.saveUrl(that.urlObj);
    });

    logoutBtn.addEventListener('click', function(e) {
      that.logoutConfirm();
    });

    document.getElementById(config.checkSelectionBtnId).addEventListener('click', function(e) {
      that.manageSelection();
    });

    document.getElementById(config.logoutConfirmBtnId).addEventListener('click', function(e) {
      localStorage.removeItem('api_key');
      logoutBtn.style.display = 'none';
      hideLoginForm(false);
      that.manageLogoutConfirmBlock(true);
    });

    document.getElementById(config.logoutNotConfirmBtnId).addEventListener('click', function(e) {
      that.manageLogoutConfirmBlock(true);
    });

    var closeBtns = document.getElementsByClassName(config.closePopupBtnId);
    for (var i = 0; i < closeBtns.length; i++) {
        closeBtns[i].addEventListener('click', function(e) {
          window.close();
        });
    }    
  }

  this.manageLogoutConfirmBlock = function(isHiding) {
    document.getElementById(config.logoutConfirmationId).style.display = (isHiding) ? "none" : "block";
  }

  this.logoutConfirm = function(type, name, email){
   that.manageLogoutConfirmBlock(false);
  }



  this.manageSelection = function() {
    chrome.tabs.executeScript( {
      code: "window.getSelection().toString();"
    }, function(selection) {
      var selection = selection ? (selection.length ? { query: selection[0] } : null ) : null;
      if (selection) {
        that.makeRequest('GET', that.buildUrl(config.baseUrl, config.api.checkSelectionUrl, selection), null, localStorage.api_key, function(http) {
          var responseObj = JSON.parse(http.response);
          if (http.readyState == 4 && http.status == 200) {
            console.log(responseObj, responseObj.message, document.getElementById(config.selectionStatusId));
            document.getElementById(config.selectionStatusId).innerHTML = responseObj.message;
            document.getElementById(config.selectionVariantsId).innerHTML = 
            responseObj.data ? (responseObj.data.length ? responseObj.data.join("\n") : "") : "";
          } else {
            if (config.status.error.indexOf(http.status) !== -1) {
              document.getElementById(config.selectionStatusId).innerHTML = responseObj.message;
            }
          }
        });
      } else {
        document.getElementById(config.selectionStatusId).innerHTML = config.messages.selectionIncorrect;
        document.getElementById(config.selectionVariantsId).innerHTML = "";
      }
    });
  }

  this.buildUrl = function(base, endpoint, params) {
    return base + endpoint + '?' + this.objectToUrlParams(params);
  }
  
  /**
   * Get the current URL.
   *
   * @param {function(string)} callback - called when the URL of the current tab
   *   is found.
  */
  this.getCurrentTabUrl = function(callback) {
    var queryInfo = {
      active: true,
      currentWindow: true
    };

    chrome.tabs.query(queryInfo, function(tabs) {
      var tab = tabs[0],
          url = tab.url;

      console.assert(typeof url == 'string', config.messages.tabUrl);
      callback(url);
    });
  }

  /**
   * Send the AJAX Request.
   *
   * @param {String} method - Get, Put, Post, Delete
   * @param {String} url - Url of Endpoint
   * @param {Object} data - Form data
   * @param {function(string)} callback - called when the request gets the responce.
  */
  this.makeRequest = function(method, url, data, token, callback) {
    this.doLoading(true);
    var xhttp;
    if (window.XMLHttpRequest) {
      xhttp = new XMLHttpRequest();
      } else {
      xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function() {
      callback(this);
      that.doLoading(false);
    };
    xhttp.open(method, url, true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    console.log(token);
    if (token) {
      xhttp.setRequestHeader('Authorization', token);
    }
    if (data) {
      xhttp.send(this.objectToUrlParams(data));
    }
    else {
      xhttp.send();
    }
  }

  /**
    * Make URL Params from the object
    *
    * @param {Object} objParams - Object that should be replaced to string
    */
  this.objectToUrlParams = function(objParams) {
    return (Object.keys(objParams).length === 0) ? null : Object.keys(objParams).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(objParams[k]);
        }).join('&');
  }

  /**
    * Login callback - hide the login form, catch the errors
    *
    * @param {Object} response - response (onreadystatechange)
    */
  this.loginCallback = function(http) {
    var errorNode = document.getElementById(config.errorNodeId);
    if (http.readyState == 4 && http.status == 200) {
      var response = JSON.parse(http.response)
      errorNode.innerHTML = '';
      localStorage.setItem('api_key', response.api_key);
      hideLoginForm(true);
      that.checkUrl();
      logoutBtn.style.display = 'block';
    } else {
      if (config.status.error.indexOf(http.status) !== -1) {
        errorNode.innerHTML = config.messages.loginError;
      }
    }
  }

  /**
    * Check the url
    */
  this.checkUrl = function() {
    that.getCurrentTabUrl(function(url) {
      var data = {
        url : url
      }
      that.urlObj = data;
      that.makeRequest('GET', that.buildUrl(config.baseUrl, config.api.searchUrl, data), null, localStorage.api_key, function(http) {
        var responseObj = JSON.parse(http.response);
        if (http.readyState == 4 && http.status == 200) {
          document.getElementById(config.statusMessageId).innerHTML = responseObj.message;
          if (responseObj.status == config.status.urlExist || responseObj.status == config.status.urlExistPending) {
            hideSendUrl();
          }
        }
      });
    });
  }

  this.saveUrl = function(urlObj) {
    that.makeRequest('POST', config.baseUrl + config.api.addUrl, urlObj, localStorage.api_key, function(http) {
      if (http.readyState == 4 && http.status == 200) {
        var responseObj = JSON.parse(http.response);
        document.getElementById(config.statusSuccessId).innerHTML = responseObj.status;
        hideSaveUrlBlock();
      } else {
        if (config.status.error.indexOf(http.status) !== -1) {
          document.getElementById(config.statusSuccessId).innerHTML = config.messages.somethingWentWrong;
        }
      }
    });
  }

  this.doLoading = function(isLoading) {
    document.getElementById(config.loadingId).style.display = (isLoading) ? 'block' : 'none';
  } 

  var hideSendUrl = function() {
    document.getElementById(config.sendUrlBlockId).style.display = 'none';
  }

  var hideSaveUrlBlock = function() {
    var successBlock = document.getElementById(config.successBlockId),
        saveUrlBlock = document.getElementById(config.saveUrlBlockId);
    saveUrlBlock.style.display = "none";
    successBlock.style.display = "block";
  }

  var hideLoginForm = function(isHideLoginForm) {
    var loginForm = document.getElementById(config.loginFormId),
        saveUrlBlock = document.getElementById(config.saveUrlBlockId),
        checkSelectionBlock = document.getElementById(config.checkSelectionBlockId);
    loginForm.style.display = (isHideLoginForm) ? "none" : "block";
    saveUrlBlock.style.display = (isHideLoginForm) ? "block" : "none";
    checkSelectionBlock.style.display = (isHideLoginForm) ? "block" : "none";
  }

  init();

}